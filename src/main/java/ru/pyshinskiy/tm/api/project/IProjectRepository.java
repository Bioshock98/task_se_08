package ru.pyshinskiy.tm.api.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.pyshinskiy.tm.entity.Project;

import java.util.List;

public interface IProjectRepository {

    @NotNull
    List<Project> findAll();

    @NotNull
    List<Project> findAll(@NotNull final String userId);

    @Nullable
    Project findOne(@NotNull final String id) throws Exception;

    @Nullable
    Project persist(@NotNull final Project project);

    @Nullable
    Project merge(@NotNull final Project merge) throws Exception;

    @Nullable
    Project findOne(@NotNull final String userId, @NotNull final String id) throws Exception;

    @Nullable
    Project remove(@NotNull final String id) throws Exception;

    @Nullable
    Project remove(@NotNull final String userId, @NotNull final String id) throws Exception;

    void removeAll(@NotNull final String userId) throws Exception;

    void removeAll();

    String getIdByNumber(final int number);

}
