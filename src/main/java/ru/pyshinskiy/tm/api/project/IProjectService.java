package ru.pyshinskiy.tm.api.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.pyshinskiy.tm.entity.Project;

import java.util.List;

public interface IProjectService {

    @Nullable
    Project findOne(@Nullable final String id) throws Exception;

    @Nullable
    Project findOne(@Nullable final String userId, @Nullable final String id) throws Exception;

    @NotNull
    List<Project> findAll();

    @Nullable
    List<Project> findAll(@Nullable final String userId);

    @Nullable
    Project persist(@Nullable final Project project);

    @Nullable
    Project merge(@Nullable final Project project) throws Exception;

    @Nullable
    Project remove(@Nullable final String id) throws Exception;

    @Nullable
    Project remove(@Nullable final String userId, @Nullable final String id) throws Exception;

    void removeAll(@Nullable final String userId) throws Exception;

    void removeAll();

    @Nullable
    String getIdByNumber(final int number);
}
