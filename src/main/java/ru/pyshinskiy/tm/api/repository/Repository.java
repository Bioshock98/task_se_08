package ru.pyshinskiy.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.List;

public interface Repository<T> {

    @NotNull
    List<T> findAll();

    @Nullable
    T findOne(@NotNull final String id) throws Exception;

    @Nullable
    T persist(@NotNull final T t);

    @Nullable
    T merge(@NotNull final T t) throws Exception;

    @Nullable
    T remove(@NotNull final String id) throws Exception;

    void removeAll();

    @Nullable
    String getIdByNumber(final int number);
}
