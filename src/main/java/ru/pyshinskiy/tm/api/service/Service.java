package ru.pyshinskiy.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.List;

public interface Service<T> {

    @Nullable
    T findOne(@Nullable final String id) throws Exception;

    @NotNull
    List<T> findAll();

    @Nullable
    T persist(@Nullable final T t);

    @Nullable
    T merge(@Nullable final T t) throws Exception;

    @Nullable
    T remove(@Nullable final String id) throws Exception;

    void removeAll();

    @Nullable
    String getIdByNumber(final int number);
}
