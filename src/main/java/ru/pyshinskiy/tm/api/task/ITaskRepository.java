package ru.pyshinskiy.tm.api.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.pyshinskiy.tm.entity.Task;

import java.util.List;

public interface ITaskRepository {

    @NotNull
    List<Task> findAll();

    @NotNull
    List<Task> findAll(@NotNull final String userId);

    @NotNull
    List<Task> findAllByProjectId(@NotNull final String userId, @NotNull final String projectId);

    @Nullable
    Task findOne(@NotNull final String id) throws Exception;

    @Nullable
    Task persist(@NotNull final Task task);

    @Nullable
    Task merge(@NotNull final Task merge) throws Exception;

    @Nullable
    Task findOne(@NotNull final String userId, @NotNull final String id) throws Exception;

    @Nullable
    Task remove(@NotNull final String id) throws Exception;

    @Nullable
    Task remove(@NotNull final String userId, @NotNull final String id) throws Exception;

    void removeAll(@NotNull final String userId) throws Exception;

    void removeAllByProjectId(@NotNull final String userId, @NotNull final String projectId) throws Exception;

    void removeAll();

    @Nullable
    String getIdByNumber(final int number);
}
