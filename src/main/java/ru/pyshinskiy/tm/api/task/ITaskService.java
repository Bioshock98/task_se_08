package ru.pyshinskiy.tm.api.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.pyshinskiy.tm.entity.Task;

import java.util.List;

public interface ITaskService {

    @Nullable
    Task findOne(@Nullable final String id) throws Exception;

    @Nullable
    Task findOne(@Nullable final String userId, @Nullable final String id) throws Exception;

    @NotNull
    List<Task> findAll();

    @NotNull
    List<Task> findAll(@Nullable final String userId);

    @Nullable
    Task persist(@Nullable final Task task);

    @Nullable
    Task merge(@Nullable final Task task) throws Exception;

    @Nullable
    Task remove(@Nullable final String id) throws Exception;

    @Nullable
    Task remove(@Nullable final String userId, @Nullable final String id) throws Exception;

    void removeAll(@Nullable final String userId) throws Exception;

    void removeAll();

    @Nullable
    String getIdByNumber(final int number);

    @NotNull
    List<Task> findAllByProjectId(@Nullable final String userId, @Nullable final String projectId);

    void removeAllByProjectId(@Nullable final String userId, @Nullable final String projectId) throws Exception;
}
