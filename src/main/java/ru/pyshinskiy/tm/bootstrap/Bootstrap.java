package ru.pyshinskiy.tm.bootstrap;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.reflections.Reflections;
import ru.pyshinskiy.tm.api.project.IProjectService;
import ru.pyshinskiy.tm.api.service.ServiceLocator;
import ru.pyshinskiy.tm.api.task.ITaskService;
import ru.pyshinskiy.tm.api.user.IUserService;
import ru.pyshinskiy.tm.command.AbstractCommand;
import ru.pyshinskiy.tm.entity.User;
import ru.pyshinskiy.tm.repository.ProjectRepository;
import ru.pyshinskiy.tm.repository.TaskRepository;
import ru.pyshinskiy.tm.repository.UserRepository;
import ru.pyshinskiy.tm.enumerated.Role;
import ru.pyshinskiy.tm.service.ProjectService;
import ru.pyshinskiy.tm.service.TaskService;
import ru.pyshinskiy.tm.service.TerminalService;
import ru.pyshinskiy.tm.service.UserService;

import java.util.*;

@NoArgsConstructor
public class Bootstrap implements ServiceLocator {

    @NotNull private final TerminalService terminalService = new TerminalService();

    @NotNull private final IProjectService projectService = new ProjectService(new ProjectRepository());

    @NotNull private final ITaskService taskService = new TaskService(new TaskRepository());

    @NotNull private final Map<String, AbstractCommand> commands = new TreeMap<>();

    @NotNull private final IUserService userService = new UserService(new UserRepository());

    @Nullable private User currentUser;

    @NotNull
    @Override
    public IProjectService getProjectService() {
        return projectService;
    }

    @NotNull
    @Override
    public ITaskService getTaskService() {
        return taskService;
    }

    @NotNull
    @Override
    public IUserService getUserService() {
        return userService;
    }

    @NotNull
    @Override
    public TerminalService getTerminalService() {
        return terminalService;
    }

    @Nullable
    @Override
    public User getCurrentUser() {
        return currentUser;
    }

    @Override
    public void setCurrentUser(@Nullable User user) {
        this.currentUser = user;
    }

    @NotNull
    @Override
    public List<AbstractCommand> getCommands() {
        return new LinkedList<>(commands.values());
    }

    public void start() throws Exception {
        createUsers();
        init();
        System.out.println("***WELCOME TO TASK MANAGER***");
        @Nullable String command;
        while(true) {
            command = terminalService.nextLine();
            execute(command);
        }
    }

    private void init() throws Exception {
        @NotNull final Reflections reflections = new Reflections("ru.pyshinskiy.tm.command");
        @NotNull final Set<Class<? extends AbstractCommand>> classes = reflections.getSubTypesOf(ru.pyshinskiy.tm.command.AbstractCommand.class);
        for (@NotNull final Class<? extends AbstractCommand> clazz : classes) {
            registry(clazz.newInstance());
        }
    }

    private void execute(@Nullable final String command) throws Exception {
        if(command == null || command.isEmpty()) return;
        if("exit".equals(command)) System.exit(1);
        final AbstractCommand abstractCommand = commands.get(command);
        if(abstractCommand == null) {
            System.out.println("UNKNOW COMMAND");
            return;
        }
        if(!abstractCommand.isAllowed(getCurrentUser())) {
            System.out.println("UNAVAILABLE COMMAND");
            return;
        }
        abstractCommand.execute();
    }

    private void registry(@NotNull final AbstractCommand command) throws Exception {
        @Nullable final String cliCommand = command.command();
        @Nullable final String cliDescription = command.description();
        if (cliCommand == null || cliCommand.isEmpty())
            throw new Exception();
        if (cliDescription == null || cliDescription.isEmpty())
            throw new Exception();
        command.setServiceLocator(this);
        commands.put(cliCommand, command);
    }

    private void createUsers() {
        @NotNull final User user = new User();
        user.setRole(Role.USER);
        user.setLogin("user");
        user.setPassword("qwerty");
        @NotNull final User admin = new User();
        admin.setRole(Role.ADMINISTRATOR);
        admin.setLogin("admin");
        admin.setPassword("1234");
        userService.persist(user);
        userService.persist(admin);
        setCurrentUser(user);
    }
}
