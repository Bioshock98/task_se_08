package ru.pyshinskiy.tm.command.project;

import org.jetbrains.annotations.NotNull;
import ru.pyshinskiy.tm.api.project.IProjectService;
import ru.pyshinskiy.tm.command.AbstractCommand;

import static ru.pyshinskiy.tm.util.entity.EntityUtil.printProjects;

public final class ProjectListCommand extends AbstractCommand {

    @Override
    @NotNull
    public String command() {
        return "project_list";
    }

    @Override
    @NotNull
    public String description() {
        return "show all projects";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[PROJECT LIST]");
        @NotNull final IProjectService projectService = serviceLocator.getProjectService();
        printProjects(projectService.findAll(serviceLocator.getCurrentUser().getId()));
        System.out.println("[OK]");
    }
}
