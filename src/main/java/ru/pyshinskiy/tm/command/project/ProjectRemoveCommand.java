package ru.pyshinskiy.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.pyshinskiy.tm.api.project.IProjectService;
import ru.pyshinskiy.tm.api.task.ITaskService;
import ru.pyshinskiy.tm.command.AbstractCommand;
import ru.pyshinskiy.tm.entity.User;

import static ru.pyshinskiy.tm.util.entity.EntityUtil.printProjects;

public final class ProjectRemoveCommand extends AbstractCommand {

    @Override
    @NotNull
    public String command() {
        return "project_remove";
    }

    @Override
    @NotNull
    public String description() {
        return "remove project and related tasks";
    }

    @Override
    public void execute() throws Exception {
        @NotNull final IProjectService projectService = serviceLocator.getProjectService();
        @NotNull final ITaskService taskService = serviceLocator.getTaskService();
        @Nullable final User currentUser = serviceLocator.getCurrentUser();
        System.out.println("[PROJECT REMOVE]");
        System.out.println("ENTER PROJECT'S ID");
        printProjects(projectService.findAll(currentUser.getId()));
        @NotNull final String projectId = projectService.getIdByNumber(Integer.parseInt(terminalService.nextLine()) - 1);
        taskService.removeAllByProjectId(currentUser.getId(), projectId);
        projectService.remove(currentUser.getId(), projectId);
        System.out.println("[PROJECT REMOVED]");
    }
}
