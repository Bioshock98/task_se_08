package ru.pyshinskiy.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.pyshinskiy.tm.api.task.ITaskService;
import ru.pyshinskiy.tm.command.AbstractCommand;
import ru.pyshinskiy.tm.entity.Task;
import ru.pyshinskiy.tm.entity.User;
import ru.pyshinskiy.tm.enumerated.Role;

import static ru.pyshinskiy.tm.util.date.DateUtil.parseDateFromString;
import static ru.pyshinskiy.tm.util.entity.EntityUtil.printTasks;

public final class TaskEditAdminCommand extends AbstractCommand {
    
    @Override
    public boolean isAllowed(@Nullable final User user) {
        if(user == null) return false;
        return Role.ADMINISTRATOR.equals(user.getRole());
    }

    @Override
    @NotNull
    public String command() {
        return "task_edit_admin";
    }

    @Override
    @NotNull
    public String description() {
        return "edit any task";
    }

    @Override
    public void execute() throws Exception {
        @NotNull final ITaskService taskService = serviceLocator.getTaskService();
        System.out.println("[TASK EDIT]");
        System.out.println("ENTER TASK ID");
        printTasks(taskService.findAll());
        @NotNull final String taskId = taskService.getIdByNumber(Integer.parseInt(terminalService.nextLine()) - 1);
        @Nullable final Task task = taskService.findOne(taskId);
        System.out.println("[ENTER TASK NAME]");
        @NotNull final Task anotherTask = new Task();
        anotherTask.setUserId(task.getUserId());
        anotherTask.setName(terminalService.nextLine());
        anotherTask.setId(task.getId());
        System.out.println("ENTER TASK DESCRIPTION");
        anotherTask.setDescription(terminalService.nextLine());
        System.out.println("ENTER START DATE");
        anotherTask.setStartDate(parseDateFromString(terminalService.nextLine()));
        System.out.println("ENTER END DATE");
        anotherTask.setEndDate(parseDateFromString(terminalService.nextLine()));
        System.out.println("[OK]");
        taskService.merge(anotherTask);
    }
}
