package ru.pyshinskiy.tm.command.task;

import org.jetbrains.annotations.NotNull;
import ru.pyshinskiy.tm.command.AbstractCommand;

import static ru.pyshinskiy.tm.util.entity.EntityUtil.printTasks;

public final class TaskListCommand extends AbstractCommand {

    @Override
    @NotNull
    public String command() {
        return "task_list";
    }

    @Override
    @NotNull
    public String description() {
        return "show all tasks";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[TASK LIST]");
        printTasks(serviceLocator.getTaskService().findAll(serviceLocator.getCurrentUser().getId()));
        System.out.println("[OK]");
    }
}
