package ru.pyshinskiy.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.pyshinskiy.tm.api.user.IUserService;
import ru.pyshinskiy.tm.command.AbstractCommand;
import ru.pyshinskiy.tm.entity.User;
import ru.pyshinskiy.tm.enumerated.Role;

import static ru.pyshinskiy.tm.util.entity.EntityUtil.printUsers;

public final class UserRemoveCommand extends AbstractCommand {

    @Override
    public boolean isAllowed(@Nullable final User user) {
        if(user == null) return false;
        return Role.ADMINISTRATOR.equals(user.getRole());
    }

    @Override
    @NotNull
    public String command() {
        return "user_remove";
    }

    @Override
    @NotNull
    public String description() {
        return "remove existing user";
    }

    @Override
    public void execute() throws Exception {
        if(Role.USER.equals(serviceLocator.getCurrentUser().getRole())) {
            System.out.println("!UNAVAILABLE COMMAND!");
        }
        else {
            System.out.println("ENTER USER ID");
            printUsers(serviceLocator.getUserService().findAll());
            @NotNull final IUserService userService = serviceLocator.getUserService();
            @NotNull final String userId = userService.getIdByNumber(Integer.parseInt(terminalService.nextLine()) - 1);
            serviceLocator.getUserService().remove(userId);
            System.out.println("USER REMOVED");
        }
    }
}
