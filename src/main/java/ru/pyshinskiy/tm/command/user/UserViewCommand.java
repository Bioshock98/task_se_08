package ru.pyshinskiy.tm.command.user;

import org.jetbrains.annotations.NotNull;
import ru.pyshinskiy.tm.command.AbstractCommand;

import static ru.pyshinskiy.tm.util.entity.EntityUtil.printUser;

public final class UserViewCommand extends AbstractCommand {

    @Override
    @NotNull
    public String command() {
        return "user_view";
    }

    @Override
    @NotNull
    public String description() {
        return "show user info";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("USER VIEW");
        printUser(serviceLocator.getCurrentUser());
        System.out.println("[OK]");
    }
}
