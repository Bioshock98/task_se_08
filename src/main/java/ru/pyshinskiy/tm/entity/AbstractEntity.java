package ru.pyshinskiy.tm.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;

import java.util.UUID;

@NoArgsConstructor
@Getter
@Setter
public abstract class AbstractEntity {

    @Nullable
    private String id = UUID.randomUUID().toString();
}
