package ru.pyshinskiy.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.pyshinskiy.tm.api.project.IProjectRepository;
import ru.pyshinskiy.tm.entity.Project;

import java.util.LinkedList;
import java.util.List;


public final class ProjectRepository extends AbstractRepository<Project> implements IProjectRepository {

    @NotNull
    @Override
    public List<Project> findAll(@NotNull final String userId) {
        @NotNull final LinkedList<Project> userProjects = new LinkedList<>();
        for(@NotNull final Project project : findAll()) {
            if(project.getUserId().equals(userId)) userProjects.add(project);
        }
        return userProjects;
    }

    @Nullable
    @Override
    public Project findOne(@NotNull final String userId, @NotNull final String id) throws Exception {
        for(@NotNull final Project project : findAll()) {
            if(project.getUserId().equals(userId)) return project;
        }
        return null;
    }

    @Nullable
    @Override
    public Project remove(@NotNull final String userId, @NotNull final String id) throws Exception {
        return entityMap.remove(id);
    }

    @Override
    public void removeAll(@NotNull final String userId) throws Exception {
        for(@NotNull final Project project : findAll()) {
            if(userId.equals(project.getUserId())) remove(project.getId());
        }
    }
}
