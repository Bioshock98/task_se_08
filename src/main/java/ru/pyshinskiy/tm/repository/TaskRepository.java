package ru.pyshinskiy.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.pyshinskiy.tm.api.task.ITaskRepository;
import ru.pyshinskiy.tm.entity.Task;

import java.util.LinkedList;
import java.util.List;

public final class TaskRepository extends AbstractRepository<Task> implements ITaskRepository {

    @NotNull
    @Override
    public List<Task> findAll(@NotNull final String userId) {
        @NotNull final LinkedList<Task> userTasks = new LinkedList<>();
        for(@NotNull final Task task : findAll()) {
            if(task.getUserId().equals(userId)) userTasks.add(task);
        }
        return userTasks;
    }

    @NotNull
    @Override
    public List<Task> findAllByProjectId(@NotNull final String userId, @NotNull final String projectId) {
        @NotNull final LinkedList<Task> projectTasks = new LinkedList<>();
        for(@NotNull final Task task : findAll(userId)) {
            if(task.getProjectId().equals(projectId)) projectTasks.add(task);
        }
        return projectTasks;
    }

    @Nullable
    @Override
    public Task findOne(@NotNull final String userId, @NotNull final String id) throws Exception {
        for(@NotNull final Task task : findAll()) {
            if(task.getUserId().equals(userId)) return task;
        }
        return null;
    }

    @Nullable
    @Override
    public Task remove(@NotNull final String userId, @NotNull final String id) throws Exception {
        return entityMap.remove(id);
    }

    @Override
    public void removeAll(@NotNull final String userId) throws Exception {
        for(@NotNull final Task task : findAll()) {
            if(userId.equals(task.getUserId())) remove(task.getId());
        }
    }

    @Override
    public void removeAllByProjectId(@NotNull final String userId, @NotNull final String projectId) throws Exception {
        for(@NotNull final Task task : findAll(userId)) {
            if(projectId.equals(task.getProjectId())) remove(userId, task.getId());
        }
    }
}
