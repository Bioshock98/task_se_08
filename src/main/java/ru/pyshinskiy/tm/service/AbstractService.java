package ru.pyshinskiy.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.pyshinskiy.tm.api.service.Service;
import ru.pyshinskiy.tm.entity.AbstractEntity;
import ru.pyshinskiy.tm.repository.AbstractRepository;

import java.util.List;

public class AbstractService<T extends AbstractEntity> implements Service<T> {

    @NotNull protected final AbstractRepository<T> abstractRepository;

    public AbstractService(@NotNull final AbstractRepository<T> abstractRepository) {
        this.abstractRepository = abstractRepository;
    }

    @Nullable
    @Override
    public T findOne(@Nullable final String id) throws Exception {
        if(id == null || id.isEmpty()) throw new Exception();
        return abstractRepository.findOne(id);
    }

    @NotNull
    @Override
    public List<T> findAll() {
        return abstractRepository.findAll();
    }

    @Nullable
    @Override
    public T persist(@Nullable final T t) {
        if(t == null) return null;
        return abstractRepository.persist(t);
    }

    @Nullable
    @Override
    public T merge(@Nullable final T t) throws Exception {
        if(t == null) return null;
        return abstractRepository.merge(t);
    }

    @Nullable
    @Override
    public T remove(@Nullable final String id) throws Exception {
        if(id == null || id.isEmpty()) throw new Exception();
        return abstractRepository.remove(id);
    }

    @Override
    public void removeAll() {
        abstractRepository.removeAll();
    }

    @Nullable
    @Override
    public String getIdByNumber(final int number) {
        return abstractRepository.getIdByNumber(number);
    }
}
