package ru.pyshinskiy.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.pyshinskiy.tm.api.project.IProjectService;
import ru.pyshinskiy.tm.entity.Project;
import ru.pyshinskiy.tm.repository.AbstractRepository;
import ru.pyshinskiy.tm.repository.ProjectRepository;

import java.util.List;

public final class ProjectService extends AbstractService<Project> implements IProjectService {
    
    @NotNull final private ProjectRepository projectRepository = (ProjectRepository) abstractRepository;
    
    public ProjectService(@NotNull final AbstractRepository<Project> projectRepository) {
        super(projectRepository);
    }

    @Nullable
    @Override
    public Project findOne(@Nullable final String userId, @Nullable final String id) throws Exception {
        if(id == null || id.isEmpty()) throw new Exception();
        if(userId == null || userId.isEmpty()) throw new Exception();
        return projectRepository.findOne(userId, id);
    }

    @Nullable
    @Override
    public List<Project> findAll(@Nullable final String userId) {
        if(userId == null) return null;
        return projectRepository.findAll(userId);
    }

    @Nullable
    @Override
    public Project remove(@Nullable final String userId, @Nullable final String id) throws Exception {
        if(userId == null || userId.isEmpty()) throw new Exception();
        if(id == null || id.isEmpty()) throw new Exception();
        return projectRepository.remove(id);
    }

    @Override
    public void removeAll(@Nullable final String userId) throws Exception {
        if(userId == null || userId.isEmpty()) throw new Exception();
        projectRepository.remove(userId);
    }
}
