package ru.pyshinskiy.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.pyshinskiy.tm.api.task.ITaskService;
import ru.pyshinskiy.tm.entity.Task;
import ru.pyshinskiy.tm.repository.AbstractRepository;
import ru.pyshinskiy.tm.repository.TaskRepository;

import java.util.List;

public final class TaskService extends AbstractService<Task> implements ITaskService {
    
    @NotNull private final TaskRepository taskRepository = (TaskRepository) abstractRepository;

    public TaskService(@NotNull final AbstractRepository<Task> abstractRepository) {
        super(abstractRepository);
    }

    @Nullable
    @Override
    public Task findOne(@Nullable final String userId, @Nullable final String id) throws Exception {
        if(id == null || id.isEmpty()) throw new Exception();
        if(userId == null || userId.isEmpty()) throw new Exception();
        return taskRepository.findOne(userId, id);
    }

    @NotNull
    @Override
    public List<Task> findAll(@Nullable final String userId) {
        if(userId == null) return null;
        return taskRepository.findAll(userId);
    }

    @Nullable
    @Override
    public Task remove(@Nullable final String userId, @Nullable final String id) throws Exception {
        if(userId == null || userId.isEmpty()) throw new Exception();
        if(id == null || id.isEmpty()) throw new Exception();
        return taskRepository.remove(id);
    }

    @Override
    public void removeAll(@Nullable final String userId) throws Exception {
        if(userId == null || userId.isEmpty()) throw new Exception();
        taskRepository.remove(userId);
    }

    @NotNull
    @Override
    public List<Task> findAllByProjectId(@Nullable final String userId, @Nullable final String projectId) {
        if(userId == null || projectId == null) return null;
        return taskRepository.findAllByProjectId(userId, projectId);
    }

    @Override
    public void removeAllByProjectId(@Nullable final String userId, @Nullable final String projectId) throws Exception {
        if(projectId == null || projectId.isEmpty()) throw new Exception();
        if(userId == null || userId.isEmpty()) throw new Exception();
        taskRepository.removeAllByProjectId(userId, projectId);
    }
}
