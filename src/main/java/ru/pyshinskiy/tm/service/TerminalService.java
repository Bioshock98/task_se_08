package ru.pyshinskiy.tm.service;

import org.jetbrains.annotations.NotNull;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public final class TerminalService {

    @NotNull private final BufferedReader input = new BufferedReader(new InputStreamReader(System.in));

    @NotNull
    public String nextLine() throws IOException {
        return input.readLine();
    }
}
