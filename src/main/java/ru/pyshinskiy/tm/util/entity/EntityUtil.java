package ru.pyshinskiy.tm.util.entity;

import org.jetbrains.annotations.NotNull;
import ru.pyshinskiy.tm.entity.Project;
import ru.pyshinskiy.tm.entity.Task;
import ru.pyshinskiy.tm.entity.User;

import java.util.List;

import static ru.pyshinskiy.tm.util.date.DateUtil.parseDateToString;

public class EntityUtil {

    public static void printProjects(@NotNull final List<Project> projects) {
        for (int i = 0; i < projects.size(); i++) {
            @NotNull final Project project = projects.get(i);
            System.out.println((i + 1) + "." + " " + project.getName());
        }
    }

    public static void printProject(@NotNull final Project project) {
        @NotNull final StringBuilder formatedProject = new StringBuilder();
        formatedProject.append("project name: ");
        formatedProject.append(project.getName());
        formatedProject.append("\nproject description: ");
        formatedProject.append(project.getDescription());
        formatedProject.append("\nstart date: ");
        formatedProject.append(parseDateToString(project.getStartDate()));
        formatedProject.append("\nend date: ");
        formatedProject.append(parseDateToString(project.getEndDate()));
        System.out.println(formatedProject);
    }

    public static void printTasks(@NotNull final List<Task> tasks) {
        for (int i = 0; i < tasks.size(); i++) {
            System.out.println((i + 1) + "." + " " + tasks.get(i).getName());
        }
    }

    public static void printTask(@NotNull final Task task) {
        @NotNull final StringBuilder formatedTask = new StringBuilder();
        formatedTask.append("task name: ");
        formatedTask.append(task.getName());
        formatedTask.append("\ntask description: ");
        formatedTask.append(task.getDescription());
        formatedTask.append("\nstart date: ");
        formatedTask.append(parseDateToString(task.getStartDate()));
        formatedTask.append("\nend date: ");
        formatedTask.append(parseDateToString(task.getEndDate()));
        System.out.println(formatedTask);
    }

    public static void printUsers(@NotNull final List<User> users) {
        for(int i = 0; i < users.size(); i++) {
            System.out.println((i + 1) + ". username: " + users.get(i).getLogin());
            System.out.println("   enumerated: " + users.get(i).getRole());
        }
    }

    public static void printUser(@NotNull final User user) {
        System.out.println("username: " + user.getLogin());
        System.out.println("enumerated: " + user.getRole());
    }
}
